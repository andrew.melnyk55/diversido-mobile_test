require 'spec_helper'

describe TablesHelper do

  it 'game_start_time returns correct value' do
    a = Array.new
    time = [50, 70, 3700]
    time.each do |t|
      a << game_start_time(t)
    end
    expect(a) ==
        ['50 sec', '1 min and 10 sec', 'It will start in more than an hour']
  end

end