require 'spec_helper'

describe User do

  it 'User with empty email should not be valid' do
    @user = User.new
    expect(@user).not_to be_valid
  end

  it 'User with non-format email should not be valid' do
    @user = User.new(email: 'qqq@iua')
    expect(@user).not_to be_valid
    @user = User.new(email: 'qqqi.ua')
    expect(@user).not_to be_valid
  end

end