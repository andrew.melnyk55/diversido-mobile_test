require 'spec_helper'

describe Table do

  it '@table respond to #active?' do
    expect(Table.new).to respond_to(:active?)
  end

  it 'active? returns positive fixnum or false' do
    table1 = Table.new(name: 'T_1', start_time: (Time.now.localtime + 10.seconds))
    table2 = Table.new(name: 'T_2', start_time: Time.now.localtime)
    expect(table2.active?).to be false
    expect(table1.active?).to be_a Fixnum
    expect(table1.active?).to be > 0
  end

end