module TablesHelper
  def game_start_time(x)
    time = ''
    if !x
      time = 'The game has already started'
    elsif (x < 60)
      time = "#{x} sec"
    elsif ((x >= 60)&&(x < 3600))
      time = "#{x / 60} min and #{x % 60} sec"
    elsif (x >= 3600)
      time = 'It will start in more than an hour'
    end
    time
  end
end