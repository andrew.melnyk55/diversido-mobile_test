module UsersHelper

  def user_tables_by_name
    @user.tables.pluck(:name).sort.join(', ')
  end

end