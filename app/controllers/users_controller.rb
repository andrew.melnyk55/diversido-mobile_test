class UsersController < ApplicationController

  def index
    @user = session[:user_id].nil? ? User.new : User.includes(:tables).find(session[:user_id])
  end

  def create
    @user = User.find_by_email(user_params[:email]) || User.create(user_params)
    if @user.errors.any?
      flash.notice = @user.errors.full_messages
    else
      attach_tables
      session[:user_id] = @user.id
    end
    redirect_to root_path
  end

  private

  def user_params
    params.permit(:email)
  end

  def attach_tables
    @user.tables.delete_all
    if params[:tables]
      @user.tables << Table.includes(:users).find(params[:tables]).select { |table| table.users.count < 6 }
    end
  end

end