class User < ActiveRecord::Base
  has_and_belongs_to_many :tables
  validates :email, presence: :true, format: { with: /.+@.+\..+/i }
end