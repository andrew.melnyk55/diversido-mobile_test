class Table < ActiveRecord::Base
  has_and_belongs_to_many :users

  def active?
    if (self.start_time - Time.now.localtime) >= 1
      (self.start_time - Time.now.localtime).round
    else
      false
    end
  end

end