class CreateTables < ActiveRecord::Migration
  def change
    create_table :tables do |t|
      t.string :name, unique: true
      t.datetime :start_time

      t.timestamps null: false
    end
  end
end