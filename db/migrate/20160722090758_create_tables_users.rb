class CreateTablesUsers < ActiveRecord::Migration
  def self.up
    create_table :tables_users, id: false do |t|
      t.references :table
      t.references :user
    end
    add_index :tables_users, [:user_id, :table_id]
    add_index :tables_users, [:table_id, :user_id]
  end
  def self.down
    drop_table :tables_users
  end
end