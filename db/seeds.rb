#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
Table.destroy_all

table_list = [
    { name: 'Table 1', start_time: (Time.now.localtime + rand(-120..4000).seconds) },
    { name: 'Table 2', start_time: (Time.now.localtime + rand(-120..4000).seconds) },
    { name: 'Table 3', start_time: (Time.now.localtime + rand(-120..4000).seconds) },
    { name: 'Table 4', start_time: (Time.now.localtime + rand(-120..4000).seconds) },
    { name: 'Table 5', start_time: (Time.now.localtime + rand(-120..4000).seconds) },
    { name: 'Table 6', start_time: (Time.now.localtime + rand(-120..4000).seconds) }
]

table_list.each do |t_hash|
  Table.create(t_hash)
end